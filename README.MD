���� ����� � ����� ������������ ��������� ���� ����.

```
data = [
    {
        ID: <string>,
        country: <string>,
        population: <number>
    },
    ...
    {
        ID: <string>,
        country: <string>,
        population: <number>
    }
]
```

������� ���������� �����

```
list = [
    {
        description: <string>,
        percentage: <number>,
        value: <number>
    },
    ...
    {
        description: <string>,
        percentage: <number>,
        value: <number>
    }
]
```

�� ����� ����� ���� ���������� �������� �������.

�� description - ����� ����� (� ����������� ������), value - ��������� ����� (� ����������� ������), percentage ��������� ����� �� ��������� �� ��������� ���� (�  ��������).

����� list �� ��������� ��������� ������ (**������ ������!**):

1. �� ���� ������������ � ������� �������� �������� percentage (����� ��������);

2. ���� � 2 �� ����� �������� � ������, � ���� ���� percentage ����� ��� OTHER_THRESHOLD, �� ��� �������� ����� ���� ����������� � ������ ����� ��������� � ����� ������.

```
{
    description: 'Other',
    percentage: <number>,
    value: <number>
}
```
